import login_google from "../../firebase/google"
import login_facebook from "../../firebase/facebook"

const Login = () => {
    const click_login_google = () => {
        login_google().then(function(result) {
            console.log(result);
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...
            }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
        });
    }
    const click_login_facebook = () => {
        login_facebook().then(function(result) {
            console.log(result);
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...
            }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
        });
    }
    var btn = (
        <div>
            <button onClick={click_login_google}>
                Login google
            </button>
            <button onClick={click_login_facebook}>
                Login facebook
            </button>
        </div>
    )
    return (
        <div>
            {btn}
        </div>
    )
}
export default Login