import Nav from '../nav';
import Login from '../login';
const Header = () => {
    return(
        <div>
            <header>
                <Nav></Nav>
                <Login></Login>
            </header>
            <style jsx>
                {`
                header{
                    background : var(--color-b1);
                    height: var(--height-header);
                    display:flex;
                    align-items:center;
                    padding:5px 15px;
                }
                `}
            </style>
        </div>
    )
}
export default Header