import Header from '../header';
import Footer from '../footer';

const content = (pros) =>{
    return (
        <div>
            <Header></Header>
            <main>
                {pros.children}
            </main>
            <Footer></Footer>
            <style jsx global>
                {`
                    :root{
                        --color-b1 : #202020;
                        --color-b2 : #181818;
                        --color-b3 : #323232;

                        --height-header : 40px;
                    }
                `}
            </style>
        </div>
    )
}
export default content