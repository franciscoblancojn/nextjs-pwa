import Link from 'next/link';
const Nav = () => {
    return (
        <div>
            <label id="check_nav_label" htmlFor="check_nav"></label>
            <input type="checkbox" id="check_nav"/>
            <nav>
                <li>
                    <Link href="/">
                        <a>
                            Inicio
                        </a>
                    </Link>
                </li>
                <li>
                    <Link href="/favoritos">
                        <a>
                            Favoritos
                        </a>
                    </Link>
                </li>
                <li>
                    <Link href="/destacados">
                        <a>
                            Destacados
                        </a>
                    </Link>
                </li>
                <li>
                    <Link href="/cuenta">
                        <a>
                            Cuenta
                        </a>
                    </Link>
                </li>
            </nav>
            <style jsx>
                {`
                    #check_nav{
                        display:none;
                    }
                    #check_nav_label{
                        width:25px;
                        height:15px;
                        display:block;
                        box-shadow: inset 0 2px white , inset 0 -2px white;
                        position:relative;
                        cursor:pointer;
                    }
                    #check_nav_label:before{
                        content:'';
                        position:absolute;
                        diplay:block;
                        width:100%;
                        height:2px;
                        background:white;
                        top:0;
                        left:0;
                        bottom:1px;
                        margin:auto;
                    }
                    nav{
                        position: fixed;
                        left: 0;
                        top: var(--height-header);
                        background: var(--color-b2);
                        height: calc(100% - var(--height-header));
                        overflow: auto;
                        padding: 15px;
                        transition: .5s;
                    }
                    #check_nav:not(:checked) + nav{
                        transform: translateX(-100%);
                    }
                `}
            </style>
        </div>
    )
}
export default Nav