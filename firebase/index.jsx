import firebase from 'firebase'
//import firebase_auth from 'firebase/auth'

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCs_POie7PA8hN7dCF79wG-_OIo7zRQUI4",
    authDomain: "testlogin-23240.firebaseapp.com",
    databaseURL: "https://testlogin-23240.firebaseio.com",
    projectId: "testlogin-23240",
    storageBucket: "testlogin-23240.appspot.com",
    messagingSenderId: "697173619176",
    appId: "1:697173619176:web:de31aa3c46a607ef49f45d",
    measurementId: "G-DKQBKP8JPF"
};

if (firebase.apps.length == 0) {
    firebase.initializeApp(firebaseConfig);
}

export default firebase