import firebase from './index'
//import firebase_auth from 'firebase/auth'

const login_facebook = () => {
    const provider = new firebase.auth.FacebookAuthProvider()
    return firebase.auth().signInWithPopup(provider)
}
export default login_facebook