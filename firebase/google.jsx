import firebase from './index'
//import firebase_auth from 'firebase/auth'

const login_google = () => {
    const provider = new firebase.auth.GoogleAuthProvider()
    return firebase.auth().signInWithPopup(provider)
}
export default login_google